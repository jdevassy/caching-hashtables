/*
 * Cache Aware HashTable
 * November 30, 2011
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<string.h>
#include<stdint.h>


#define TRUE 1
#define FALSE !TRUE
#define SIZE 200000							    /* defines the number of entries the hashtable can hold */
#define SIZE_OF_FILE 100000                     /* defines the number of records present in the database to be hashed */
#define NUM_COLUMNS 4 							/* defines the number of columns in the database to be hashed */
#define NUM_USERS 943							/* number of users in database */
#define NUM_MOVIES 1682							/* number of movies in database */
#define HASH_CONSTANT ((sqrt(5) - 1) / 2)

#define B 4096 //for a LINUX Ubuntu  11.4 32-bit distro
#define CLS 128 //for a LINUX Ubuntu 11.4 32-bit distro 

/* Struct definition for HashTable */
typedef 
struct HT
{
    int user_id;
    int movie_id;
    int rating;
    int padding;
} HT;

/* globals */
HT* hashtable;
int J=0;
int fetches = 0;
int just_previously_inserted_userid=0;
int just_previously_inserted_movieid=0;

/* FUNCTIONS */
int hash(int userid, int movieid)
{
        return (SIZE * fmod(((userid+movieid) * HASH_CONSTANT),1));
}


// calculate the CPU cycles
uint64_t get_CPU_cycle_counter()
{
        uint64_t x;
        __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
        return x;
}

// distance between two indices in the hashtable
int distance(int pos1, int pos2)
{
    return (pos1-pos2);
}

// insert into hashtable by blockedProbing
void blockedProbingInsert(int userid, int movieid, int rating, int just_userid, int just_movieid)
{
    int i=0, k=0;
    int probe;
    HT present;

    probe = hash(userid, movieid);
    while(TRUE)
    {
        for(i=0;i<CLS;i++)
        {        
            //for(k=0;k<pow(2,i);k++)
            for(k=0;k<=i;k++)
            {
                if(hashtable[probe+k].user_id == 0) //found an empty slot in the hashtable
                {
                    hashtable[probe+k].user_id = userid;
                    hashtable[probe+k].movie_id = movieid;
                    hashtable[probe+k].rating = rating;
                    hashtable[probe+k].padding=0;
                    return;
                }
                else
                {
                    present = hashtable[probe+k];
                    if( distance(probe+k, hash(present.user_id, present.movie_id)) > CLS-1 )
                    {
                        if(present.user_id == just_userid && present.movie_id == just_movieid)
                        {
                            i=CLS;
                            break;
                        }

                        hashtable[probe+k].user_id = userid;
                        hashtable[probe+k].movie_id = movieid;
                        hashtable[probe+k].rating = rating;
                        hashtable[probe+k].padding=0;

                        just_previously_inserted_userid = userid;
                        just_previously_inserted_movieid = movieid;

                        blockedProbingInsert(present.user_id, present.movie_id, present.rating, just_previously_inserted_userid, just_previously_inserted_movieid);
                        return;
                    }
                }
            }
        }

        // control reaches here when there is a key that cannot be placed in the hashtable 
        // such that it can be retrieved in a maximum of 2 I/Os. 
        // allowing it to be retrieved i more than 2 I/Os temporarily (this key may be replaced in future by another key)
        probe = probe + CLS;

        // there is not any location to place this key 
        if(probe > SIZE-1)
        {
            printf("Error: Could not insert the key with user id: %d and movie id: %d into the hashtable!\n", userid, movieid);
            getchar();
            return;
        }

        just_previously_inserted_userid = 0;
        just_previously_inserted_movieid = 0;
            
    } //end of while(TRUE)
}

// search the hashtable for a key using blockedProbing
int blockedProbingSearch(int userid, int movieid)
{
    int i=0, k=0;
    int probe;
    HT present;

    probe = hash(userid, movieid);

    for(i=0;i<CLS;i++)
    {
        for(k=0;k<=i;k++)
        {
            if((hashtable[probe+k].user_id==userid) && (hashtable[probe+k].movie_id==movieid))
            {
                return (probe+k);
            }
            else
            {
                present = hashtable[probe+k];
                if(present.user_id == 0)
                    continue;
                if( distance(probe+k, hash(present.user_id, present.movie_id)) > CLS-1 )
                {
                    return -1;
                }
            }
        }
    }

    return -1;
}

int searchTenKeys()
{
    static int callCount=0;
    int i=0, searchVerifier=0;
    uint64_t start_cycle, end_cycle, diff=0;

    int searchUserIds[10]={115, 178, 178, 189, 189, 279, 279, 1, 1, 130};
    int searchMovieIds[10]={265, 265, 332, 332, 512, 512, 1336, 1336, 61, 61};

    ++callCount;

    for(i=0;i<10;i++)
    {
        start_cycle = get_CPU_cycle_counter();
        if(blockedProbingSearch(searchUserIds[i], searchMovieIds[i]) != -1)
        {
            printf("Found user id %d, movie id %d!\n", searchUserIds[i], searchMovieIds[i]);
            ++searchVerifier;
        }
        else
        {
            printf("User id %d, Movie id %d not found!\n", searchUserIds[i], searchMovieIds[i]);
            --searchVerifier;
        }
        end_cycle = get_CPU_cycle_counter();
        diff += end_cycle - start_cycle;
    }

    diff = diff/10;

    if(callCount == 100 && searchVerifier != 0)
    {
        printf("Error in your search algorithm!!!!");
        getchar();
    }

    return diff;
}

// check if a number is a power of 2
int isPowerOfTwo(int x)
{
    return ((x != 0) && !(x & (x - 1)));
}


/* MAIN FUNCTION */
int main (int argc, char const *argv[])
{
    // Cache-Oblivious Hash Table USING IMDB data
    
    /* declaration of index and count variables */
    int i=0;
    int j=0;
    int count = 0;

    /* declaration of variables pertaining to data to be hashed */
    char* readToken = (char*)malloc(100*sizeof(char));
    char fname[]="./u.data";
    char fout[] = "./o.data";
    int* users = (int*)malloc(SIZE_OF_FILE*sizeof(int));
    int* movies = (int*)malloc(SIZE_OF_FILE*sizeof(int));
    int* ratings = (int*)malloc(SIZE_OF_FILE*sizeof(int));
   
    /* declaration of variables pertaining to performance measurement */
    uint64_t elapsed_cycles;

    //Calculating the number of hashtable entries that can be read in 1 I/O


    J = B/sizeof(HT);
    printf("size of HT %d", sizeof(HT));
    printf("J is %d", J);
    printf("CLS is %d", CLS);
    fetches = J/CLS;
    printf("Number of fetches %d", fetches);

    //Opening the input file to read, and the outputfile to write
    FILE* fp = fopen(fname,"r");
    FILE* fpo = fopen(fout,"w");

    if(!fp)
    {
        printf("Error: Cannot open input file %s for reading!\n",fname);
        return 1;
    }


    if(!fpo)
    {
        printf("Error: Cannot open output file %s for writing!\n",fout);
        return 1;
    }

    //Creating the hashtable
    hashtable = (HT*)malloc(SIZE*sizeof(HT));
    printf("Address at which hashtable starts = %p\n",(void*)hashtable);

    //Reading the data file word by word and storing in users, movies and ratings arrays
    printf("Reading word by word:\n");
    while(TRUE)
    {
        fscanf(fp,"%s",readToken);

        if(!strcmp(readToken,"END"))
		break;
        ++count;

        if(count%NUM_COLUMNS == 0)
	    {
		    ++j;
		    continue;
	    }
	    else if(count%NUM_COLUMNS == 1)
	    {
		    //store in users array
		    users[j] = atoi(readToken);	
	    }
	    else if(count%NUM_COLUMNS == 2)
	    {
		    //store in movies array
		    movies[j] = atoi(readToken);
	    }
	    else
	    {
		    //store in ratings array
		    ratings[j] = atoi(readToken);
	    }
    }
    printf("End of reading file - read %d tokens!\n",count);
    //printf("For verification: \n users[99999] = %d \t movies[99999] = %d \t ratings[99999] = %d\n",users[99999],movies[99999],ratings[99999]);

    //Inserting by blockedProbing the elements in database into hashtable
    i=0;
    for(j=0;j<SIZE_OF_FILE;j++)
    {
        blockedProbingInsert(users[j],movies[j],ratings[j],0,0);
    	if((j+1)%100 == 0)
	    {
	        printf("\nPerforming an intermediary lookup for 10 <userid,movieid> pairs . . . \n");
	        elapsed_cycles = searchTenKeys();
	        fprintf(fpo,"%llu\n",elapsed_cycles);
	        ++i;
	    }
    }

    fclose(fpo);
    fclose(fp);

    return 0;
} 

