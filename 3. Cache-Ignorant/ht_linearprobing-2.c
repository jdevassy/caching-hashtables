/*
 * Cache Ignorant HashTable with Open Addressing
 * November 7, 2011 
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<string.h>
#include<stdint.h>


#define TRUE 1
#define FALSE !TRUE
#define SIZE 150000							    /* defines the number of entries the hashtable can hold */
#define SIZE_OF_FILE 100000                     /* defines the number of records present in the database to be hashed */
#define NUM_COLUMNS 4 							/* defines the number of columns in the database to be hashed */
#define NUM_USERS 943							/* number of users in database */
#define NUM_MOVIES 1682							/* number of movies in database */
#define HASH_CONSTANT ((sqrt(5) - 1) / 2)

/* Struct definition for HashTable */
typedef 
struct HT
{
    int user_id;
    int movie_id;
    int rating;
} HT;

/* globals */
HT* hashtable[SIZE];                        

/* FUNCTIONS */
int hash(int userid, int movieid)
{
    return (SIZE * fmod(((userid+movieid) * HASH_CONSTANT),1));
}


// calculate the CPU cycles
uint64_t get_CPU_cycle_counter()
{
    uint64_t x;
    __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
    return x;
}

// insert into hashtable by LinearProbing
void linearProbingInsert(int userid, int movieid, int rating)
{
    int probe = hash(userid, movieid);

    while(hashtable[probe]->user_id!=0)
    {                            
        probe = fmod((probe+1),SIZE);                  
    }

    hashtable[probe]->user_id = userid;
    hashtable[probe]->movie_id = movieid;
    hashtable[probe]->rating = rating; 
}

// search the hashtable for a key using LinearProbing
int linearProbingSearch(int userid, int movieid)
{
    int probe = hash(userid,movieid);                                  
    int i;
    for(i=0;i<SIZE;i++)
    {                               
        if((hashtable[probe]->user_id==userid) && (hashtable[probe]->movie_id==movieid))
        {
           printf("\n userid %d and movieid %d are present at the %d th location in the hashtable, and the rating is %d\n", userid, movieid, probe, hashtable[probe]->rating);
           return probe;
        }
        probe = fmod((probe+1),SIZE);                  
    }
    return -1;                                              
}

int searchTenKeys()
{
    static int callCount=0;
    int i=0, searchVerifier=0;
    uint64_t start_cycle, end_cycle, diff=0;

    int searchUserIds[10]={115, 178, 178, 189, 189, 279, 279, 1, 1, 130};
    int searchMovieIds[10]={265, 265, 332, 332, 512, 512, 1336, 1336, 61, 61};

    ++callCount;

    for(i=0;i<10;i++)
    {
        start_cycle = get_CPU_cycle_counter();
        if(linearProbingSearch(searchUserIds[i], searchMovieIds[i]) != -1)
        {
            printf("Found user id %d, movie id %d!\n", searchUserIds[i], searchMovieIds[i]);
            ++searchVerifier;
        }
        else
        {
            printf("User id %d, Movie id %d not found!\n", searchUserIds[i], searchMovieIds[i]);
            --searchVerifier;
        }
        end_cycle = get_CPU_cycle_counter();
        diff += end_cycle - start_cycle;
    }

    diff = diff/10;

    if(callCount == 100 && searchVerifier != 0)
    {
        printf("Error in your search algorithm!!!!");
        getchar();
    }

    return diff;
}


/* MAIN FUNCTION */
int main (int argc, char const *argv[])
{
    // Cache-Ignorant Hash Table USING IMDB data

    /* declaration of index and count variables */
    int i=0;
    int j=0;
    int count = 0;

    /* declaration of variables pertaining to data to be hashed */
    char* readToken = (char*)malloc(100*sizeof(char));
    char fname[]="./u.data";
    char fout[] = "./o.data";
    int* users = (int*)malloc(SIZE_OF_FILE*sizeof(int));
    int* movies = (int*)malloc(SIZE_OF_FILE*sizeof(int));
    int* ratings = (int*)malloc(SIZE_OF_FILE*sizeof(int));

    /* declaration of variables pertaining to performance measurement */
    uint64_t elapsed_cycles;

    //Opening the input file to read, and the outputfile to write
    FILE* fp = fopen(fname,"r");
    FILE* fpo = fopen(fout,"w");

    if(!fp)
    {
        printf("Error: Cannot open input file %s for reading!\n",fname);
        return 1;
    }


    if(!fpo)
    {
        printf("Error: Cannot open output file %s for writing!\n",fout);
        return 1;
    }

    //Creating the hashtable
    for(i=0;i<SIZE;i++)
    {
        hashtable[i] = (HT*)malloc(sizeof(HT)); 
    }

    //Reading the data file word by word and storing in users, movies and ratings arrays
    printf("Reading word by word:\n");
    while(TRUE)
    {
        fscanf(fp,"%s",readToken);

        if(!strcmp(readToken,"END"))
		break;
        ++count;

        if(count%NUM_COLUMNS == 0)
	{
		++j;
		continue;
	}
	else if(count%NUM_COLUMNS == 1)
	{
		//store in users array
		users[j] = atoi(readToken);	
	}
	else if(count%NUM_COLUMNS == 2)
	{
		//store in movies array
		movies[j] = atoi(readToken);
	}
	else
	{
		//store in ratings array
		ratings[j] = atoi(readToken);
	}
    }
    printf("End of reading file - read %d tokens!\n",count);
    //printf("For verification: \n users[99999] = %d \t movies[99999] = %d \t ratings[99999] = %d\n",users[99999],movies[99999],ratings[99999]);

    //Inserting by linearProbing the elements in database into hashtable
    i=0;
    for(j=0;j<SIZE_OF_FILE;j++)
    {
        linearProbingInsert(users[j],movies[j],ratings[j]);
	    if((j+1)%100 == 0)
	    {
	        printf("\nPerforming an intermediary lookup for 10 <userid,movieid> pairs . . . \n");
	        elapsed_cycles = searchTenKeys();
	        fprintf(fpo,"%llu\n",elapsed_cycles);
	        ++i;
	    }
    }

    fclose(fpo);
    fclose(fp);

    return 0;
}

